==========
Objective: Identify the phases of an Ajax call
==========

[[
You are building a Web 2.0 application using the built-in
<code>XMLHttpRequest</code> API.

Which JavaScript code snippet tests when the Ajax <code>request</code>
has completed and has an HTTP status of <code>OK</code>?
]]
1: <pre>if (request.isComplete()
            && request.isStatusOK()) {
  // process the response
}</pre>
*2: <pre>if (request.readyState == 4
            && request.status == 200) {
  // process the response
}</pre>
3: <pre>if (request.getReadyState() == 4
            && request.getHttpStatus() == 200) {
  // process the response
}</pre>
4: <pre>if (request.readyState == XMLHttpRequest.COMPLETE
            && request.status == XMLHttpRequest.HTTP_OK) {
  // process the response
}</pre>


======
Objective: Describe use cases for uses Ajax
======

[[
Which statement describes the purpose of Ajax technology.
]]
1:  Ajax is used to cleanse JavaScript code for use in strict browsers
*2: Ajax is used to perform HTTP requests without rendering a completely new page
3:  Ajax is used to minimize and obsfucate JavaScript code for production deployment
4:  Ajax is used bridge between Java applets and the rest of the web page via a JavaScript API
