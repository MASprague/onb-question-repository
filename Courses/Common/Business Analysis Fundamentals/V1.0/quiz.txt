

(Type): multiplechoice
(Category): 15
(Question): What is the term used for those who have a vested interest in the project's outcome?
(A):  Stakeholder
(B):  Shareholder
(C):  Technical Lead
(D):  Project Manager
(E):  Upper Management
(Correct): A
(Points): 1
(WF): Stakeholders are those who may come from varying disciplines, who have a vested interest in the product's outcome.


(Type): multiplechoice
(Category): 15
(Question): What makes a business analyst well suited to manage testing?
(A):  They work closely with the scrum master
(B):  They usually come from testing backgrounds
(C): They are familiar with the details of the technology
(D):  They have significant knowledge of the people and processes involved in the organization
(Correct): D
(Points): 1
(WF): The business analyst doesn't need knowledge of specific technology details or have testing experience.  They are aware of a broader picture.



(Type): multiplechoice
(Category): 15
(Question): What is at the core of a good business analyst mindset?
(A):  Time lines
(B):  Budget limitations
(C): Being able to ask "Why?"
(D):  Programmatic knowledge
(Correct): C
(Points): 1
(WF): The core of a business analyst is know when something is or is not needed.


(Type): multiplechoice
(Category): 15
 (Question): What is Swivel Technology?
(A):  Any Agile tracking software
(B):  A flexible software that serves multiple purposes
(C):  When a user is able to stay in the same software for their entire job
(D): When a computer user gathers information from one system but enters it in another
 (Correct): D
(Points): 1


(Type): multiplechoice
(Category): 15
 (Question): Which two methods are work best for setting targets? (Choose two)
(A): Be specific
(B):  Share starting point
(C):  Ask the stakeholders for an end date
(D):  Keep a whiteboard of completed user stories
 (Correct): A,B
(Points): 1
(WF): Knowing what is done doesn't set a target.  Having an external force assign an end date is not Agile.


(Type): multiplechoice
(Category): 15
 (Question): Requirements define _______ is needed, not the ______.
(A): who, how
(B): what, why
(C): what, how
(D): who, when
(Correct): C
(Points): 1
 (WF): Requirements define what is needed, not the how.


(Type): multiplechoice
(Category): 15
 (Question): This is one of the top 3 reasons for project success...
(A): User involvement
(B): Disabling features
(C): Project management
(D): Training stakeholders
(Correct): A
(Points): 1
 (WF): User involvement is one of the top 3 reasons for project success (according to the lynda.com course)


(Type): true/false
(Category): 15
 (Question): A business analyst must be proficient in both business and technical language in order to facilitate between these groups. 
(A): True
(B): False
(Correct): A
(Points): 1
 (WF): See Part 1 - Adopting a business analyst mindset


(Type): multiplechoice
(Category): 15
(Question): What does the SMART acronym stand for?
(A): Search, Model, Add, Remove, Transition
(B): Specific, Measurable, Achievable, Realistic, Traceable
(C): Scalable, Marketable, Attainable, Retraceable, Timely
(D): Standardized, Maintainable, Achievable, Realistic, Testable
(E): Set targets, Make a plan, Achieve milestones, Revisit problems, Test results
 (Correct): B
(Points): 1
 (WF): The SMART standard for requirements is an acronym for Specific, Measurable, Achievable, Realistic, and Traceable.


(Type): multiplechoice
(Category): 15 
 (Question): What is the smallest, topmost level at the tip of the requirements pyramid?
(A): Testing
(B): Execution
(C): Project Need
(D): Requirements
(E): Specs and Design
(Correct): C
(Points): 1
 (WF): The top level of the requirements pyramid is Project Need.


(Type): multiplechoice
(Category): 15
 (Question): Which is of the following is NOT good advice for brainstorming?
(A): Write all ideas down
(B): State objectives beforehand
(C): Do not set a strict time limit
(D): Strive for a high volume of ideas
(E): Encourage piggy-backing of ideas
(Correct): C
(Points): 1
 (WF): Brainstorming sessions should be time-boxed, preferably about 20 minutes.


(Type): multiplechoice
(Category): 15
 (Question): When prioritizing requirements, what are helpful categories to use to group requirements of similar importance?
(A): Implied and explicit
(B): Basic and advanced
(C): Critical and non-critical
(D): Functional and non-functional
(E): Mandatory, important, and nice-to-have
 (Correct): E
(Points): 1
 (WF): Use the categories mandatory, important, and nice-to-have to prioritize requirements.


(Type): truefalse
(Category): 15
 (Question): When validating a requirement with 10 reviewers, it is not necessary for all to believe the requirement would bring business value.
(A): True
(B): False
(Correct): A
(Points): 1
 (WF): 75% of reviewers should believe the requirement would bring business value.


(Type): multiplechoice
(Category): 15
 (Question): When making observations during requirements gathering, what two things should a business analyst do? (Choose 2)
(A): Design the solution
(B): Look for manual steps
(C): Look for swivel technology
 (D): Concentrate exclusively on the solution that the stakeholders describe
 (Correct): B,C
(Points): 1
 (WF): A goal of the requirements process is to define "what" is needed to be done, not the solution or "how" it should be done.


(Type): multiplechoice
(Category): 15
 (Question): You are documenting the way data will be transformed from an existing system in order to become more compatible with the new system.  What type of requirement are you documenting?
(A): Solution Requirements
(B): Transition Requirements
(C): Functional Requirements
(D): Non-functional Requirements
(Correct): B
(Points): 1
 (WF): Transition requirements cover data conversion from existing systems, skill gaps that must be addresses, and other related changes to reach the desired future state.


(Type): multiplechoice
(Category): 15
 (Question): At which point of a project would you see an increase in the submission of change requests?
(A): During the project's launch
(B): Towards the end of the project
(C): Towards the beginning of the project
(D): During the project scope management process
 (Correct): B
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): Which attribute is NOT part of the standard business analysis process?
(A): Team roles
(B): Deliverables
(C): Analysis technique
(D): Requirements for solution acceptance
(Correct): D
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): You have been asked to create a RACI chart for a new project.  Which definition below best describes a RACI chart?
(A): It's a matrix that uses Role, Action, Consult, and Inform as part of the conduct stakeholder analysis process
(B): It's a role and responsibility chart that identifies when stakeholders are needed in the business analyst duties
(C): It's a matrix that uses the Responsible, Accountable, Consult, and Inform tasks as part of the stakeholder analysis process
(D): It's a rule that the business analyst can use to identify all of the needed stakeholders: Roles, Actions, Communications, and Interest
(Correct): C
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): Which statement best describes the process of requirements analysis? 
(A): It is conformity to requirements and a fitness for use
(B): It defines all of the work, and only the required work, to complete project objectives
(C): It ensures that analysis and implementation efforts focus on the most critical requirements
(D): It covers the definition of stakeholder requirements, which describe what a solution must be capable of doing
(Correct): D
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
(Question): When conducting a stakeholder analysis which two factors are the most important? 
(A): Position and politics
(B): Politics and influence
(C): Attitude and position
(D): Influence and attitude
(Correct): D
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): Which plan would be created to define the scope and requirements of the solution?
(A): Requirements plan
(B): Business analysis plan
(C): Risk management plan
(D): Requirements management plan
(Correct): D
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
(Question): Why is it preferred to front-load the most risky requirements first while doing prioritization?
(A): If the risk comes true the project manager can mitigate the risk event
(B): If the risk comes true then there is ample time to correct the problem
(C): If the risk comes true the project will fail with little work invested
(D): If the risk comes true the project team will be rewarded as soon as possible
 (Correct): C
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): Which statement best describes the goals and objectives of a project?  
(A): They describe the ends that the organization is seeking to achieve
(B): They describe all of the benefits in comparison to the cost and risks of the project
(C): They describe the processes the solution will need to improve for the project to be successful
 (D): They describe all of the work that needs to be completed in order to reach the project goals and objectives
 (Correct): A
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): Which of the examples below is NOT an example of a desired business outcome?
(A): Reduce costs
(B): Increase sales
(C): Reduce the time to deliver a product or service
(D): Implement a new machine to complete the work process faster
(Correct): D
(Points): 1
 (WF): 


(Type): multiplechoice
(Category): 15
 (Question): A new software project has been assigned to you that will affect the 2,000 administrative assistants across the organization.  As the business analyst for this project you need to understand the requirements for scheduling meetings, reserving facilities, and sharing calendars across the 2,000 administrative assistants.  Which approach would be the best for gathering the requirements?
 (A): Meet with all of the administrative assistants as part of requirements elicitation 
(B): Send out a survey to every administrative assistant asking for their requirements
(C): Meet with the managers of the administrative assistance since this will be a more manageable amount of people
(D): Meet with a small group of administrative assistants that will serve as representatives for the remaining administrative assistants
(E): Meet with a small group of administrative assistants and assume that their requirements reflect the requirements of all administrative assistants
 (Correct): D
(Points): 1
 (WF):
