(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Victor Oyedeji
(Course Site): Udemy
(Course Name): Communication Skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication 
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): When conducting a meeting, if someone you require to be at the meeting does not show up, what should you do? (Select 2)
(A): Wait for that person before starting, even if it means starting a little late.
(B): Start the meeting on time
(C): Brief the individual on what he/she missed after the meeting.
(D): Cancel the meeting
(Correct): B,C
(Points): 2
(CF): You should start the meeting on time, and brief the individual on what he/she missed after the meeting.
(WF): You should start the meeting on time, and brief the individual on what he/she missed after the meeting.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What does it mean to "get buy-in"?
(A): Buying a presentation product
(B): Starting the meeting on time
(C): Know where people are, in order see if you should move forward in your presentation
(D): End the meeting on time
(E): Getting the manager into your presentation and confirm if your material is correct
(Correct): C
(Points): 1
(CF): "Get buy-in" means to make sure to know where, in order see if you should move forward in your presentation.
(WF): "Get buy-in" means to make sure to know where, in order see if you should move forward in your presentation.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): Explain the 3x3 for all phone calls. (Select 2)
(A): Email the agenda and purpose to all attendees after completing the call.
(B): Email the agenda and purpose to all attendees while planning the call.
(C): Call the participants by 3 prior to the call.
(D): Email the materials to all attendees just prior to the call.
(E): Email the materials to all attendees’ right after the call.
(Correct): B,D
(Points): 2
(CF): Email the agenda and purpose to all attendees while planning the call, and email the materials to all attendees just prior to the call.
(WF): Email the agenda and purpose to all attendees while planning the call, and email the materials to all attendees just prior to the call.
(STARTIGNORE)
(Hint):
(Subject): Communications Conference Call
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is one important common aspect to ALL communication flows (email, presentations, people, etc.)
(A): Share purpose and intended results for that meeting to all participants.
(B): Take notes with your computer.
(C): Share the notes with the entire team, regardless if they participated in the meeting or not.
(D): Have long introductions at the beginning in the meeting.
(Correct): A
(Points): 1
(CF): One important common aspect, is to share purpose and intended results for that meeting to all participants.
(WF): One important common aspect, is to share purpose and intended results for that meeting to all participants.
(STARTIGNORE)
(Hint):
(Subject): Communications Conference Call
(Difficulty): Advanced  
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is the average attention span for adults when discussing a topic?
(A): 5 minutes
(B): 6 minutes
(C): 7 minutes
(D): 8 minutes
(E): 9 minutes
(Correct): D
(Points): 1
(CF): The average attention span for adults is 8 minutes.
(WF): The average attention span for adults is 8 minutes.
(STARTIGNORE)
(Hint):
(Subject): Communications 
(Difficulty): Beginner  
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): If your email covers multiple topics, what should be standard practice? (Select 2)
(A): Keep multiple topics in the email
(B): Keep each topic detailed within the email
(C): You should send one topic per email
(D): Use the correct font and style within the email
(Correct): C,D
(Points): 2
(CF): You should send one topic per email, all while using the correct font and style within the email.
(WF): You should send one topic per email, all while using the correct font and style within the email.
(STARTIGNORE)
(Hint):
(Subject): Communications Email 
(Difficulty): Beginner  
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What type of fonts are harder to read in a PowerPoint presentation?
(A): Arial
(B): Italicized
(C): Bold
(D): Helvetica
(E): Sans-Serif
(Correct): B
(Points): 1
(CF): Italicized fonts are harder to read in a PowerPoint presentation.
(WF): Italicized fonts are harder to read in a PowerPoint presentation.
(STARTIGNORE)
(Hint):
(Subject): Communications PowerPoint 
(Difficulty): Intermediate 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 1
(Question): What should you accomplish within the body of a presentation? (Select 3)
(A): Support an outcome
(B): Create a resolution
(C): Direct and Hold their attention
(D): Connect with the audience
(E): Ask for a call back after the presentation
(Correct): A,C,D
(Points): 3
(CF): A,C and D are correct.
(WF): A,C and D are correct.
(STARTIGNORE)
(Hint):
(Subject): Communications PowerPoint 
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): On average, consultants spend ____ of their time on email each week.
(A): 50%
(B): 10%
(C): 60%
(D): 38%
(E): 28%
(Correct): E
(Points): 1
(CF): On average, consultants spend 28% of their time on email each week.
(WF): On average, consultants spend 28% of their time on email each week.
(STARTIGNORE)
(Hint):
(Subject): Communications email
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): Project managers expect you to understand the goals when talking to them.
(A): True
(B): False 
(Correct): B
(Points): 1
(CF): Correct. Project managers expect you to understand the PLAN when talking to them.
(WF): False. Project managers expect you to understand the PLAN when talking to them.
(STARTIGNORE)
(Hint):
(Subject): Communications People
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What part of the brain deals with decisions when you meet with a consultant?
(A): Lizard
(B): Mammal 
(C): Human
(Correct): B
(Points): 1
(CF): The Mammal part of the brain deals with decisions while meeting with a consultant.
(WF): The Mammal part of the brain deals with decisions while meeting with a consultant.
(STARTIGNORE)
(Hint):
(Subject): Communications People
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
