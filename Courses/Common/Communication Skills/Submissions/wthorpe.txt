(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Bill Thorpe
(Course Site): udemy
(Course Name): communications skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication/
(Discipline): professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is the attention span of an average adult according to the course?
(A): 5 minutes
(B): 8 minutes
(C): 9 minutes
(D): Varies based on the presentation.
(E): 6 minutes
(Correct): B
(Points): 1
(CF): The average adult has a very short attention span, 8 minutes was mentioned multiple times.
(WF): The average adult has a very short attention span, 8 minutes was mentioned multiple times.
(STARTIGNORE)
(Hint):
(Subject): Communications
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When attending a meeting, upon whom should you focus your attention?
(A): Whoever is currently speaking
(B): The meeting chairperson
(C): Your direct superior
(D): Whatever member is the focus of the current topic
(Correct): B
(Points): 1
(CF): The chairperson of the meeting should be the focus of your attention.  If one is not specified, the person who sent out the agenda is a good second choice
(WF): The chairperson of the meeting should be the focus of your attention.  If one is not specified, the person who sent out the agenda is a good second choice
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When not all invited members of the meeting are present at the starting time, how long should you wait to start?
(A): No more than 5 minutes
(B): Until everyone arrives
(C): Start immediately
(D): Until the most senior invitee arrives
(Correct): C
(Points): 1
(CF): You should always start on time.  You can fill in latecomers after the meeting.
(WF): You should always start on time.  You can fill in latecomers after the meeting.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When someone comes into your workspace with an issue while you are on a conference call, what is the most appropriate response?
(A): Place the call on hold and help that person as long as needed.
(B): Place the call on hold and quickly ask the person to come back after the meeting
(C): Mute the call and work with the person quickly.
(D): Hang up so other attendees are aware that you had something pressing come up.
(Correct): C
(Points): 1
(CF): Never use hold, it will often flood the conference call with hold music.  hanging up is generally poor etiquette.
(WF): Never use hold, it will often flood the conference call with hold music.  hanging up is generally poor etiquette.
(STARTIGNORE)
(Hint):
(Subject): Communications Conference calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When creating an email, which of the following statements is true?
(A): Take your time writing a response to a serious message to be sure you have covered all bases.
(B): You should include as little extra as possible, only covering the important facts
(C): You should follow up on an unanswered email no earlier than 24 hours later.
(D): You should reread an email that was written when angry carefully for possible errors.
(Correct): B
(Points): 1
(CF): important emails should be answered quickly, you should not resend checking on unanswered emails, and you should never write an angry email.
(WF): important emails should be answered quickly, you should not resend checking on unanswered emails, and you should never write an angry email.
(STARTIGNORE)
(Hint):
(Subject): Communications Email
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): The easiest fonts to read for most people are?
(A): Serif Fonts
(B): San-Serif Fonts
(C): Script Fonts
(D): Italic Fonts
(Correct): B
(Points): 1
(CF): San-serif fonts include Arial or Helvetica.  Other types are harder to read.
(WF): San-serif fonts include Arial or Helvetica.  Other types are harder to read.
(STARTIGNORE)
(Hint):
(Subject): Communications Presentations
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When giving a presentation, what should you not do?
(A): Vary your tone between animated and slow
(B): Surprise the audience with startling statements or sudden slides
(C): Tell a story you feel is related to your presentation
(D): Maintain a calm even tone that does not vary.
(Correct): D
(Points): 1
(CF): Unvaried tone, even if calm, tends to be boring.
(WF): Unvaried tone, even if calm, tends to be boring.
(STARTIGNORE)
(Hint):
(Subject): Communications presentations
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Using the Triune brain theory, when you meet with an executive, what part of his brain tends to be more pronounced?
(A): Lizard Brain
(B): Mammal Brain
(C): Human Brain
(D): All parts are fairly equal.
(Correct): C
(Points): 1
(CF): The mammal brain is the one used for logic and long term decision making.
(WF): The mammal brain is the one used for logic and long term decision making.
(STARTIGNORE)
(Hint):
(Subject): Communications
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 2
(Random answers): 1
(Question): When communicating with someone from a culture you are not familiar with, what sort of things should you avoid?
(A): Speaking very slowly to help them understand you
(B): Using very common acronyms
(C): Writing things down for them
(D): Smiling a lot and using humorous anecdotes.
(Correct): B, C
(Points): 1
(CF): Many acronyms do not translate well because common names start with different letters.  Similarly, humor translates poorly.
(WF): Many acronyms do not translate well because common names start with different letters.  Similarly, humor translates poorly.
(STARTIGNORE)
(Hint):
(Subject): Communications Foreign
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When someone is aggravated at you about a perceived mistake, how should you react?
(A): Be polite, but firmly reinforce that the mistake was not yours
(B): Use mirroring technique to mimic his anger and let him work it out
(C): Listen carefully, give direct answers to questions, try to convert him to your point of view.
(D): Listen to what they say, document it, and show it to someone above him to keep him from interfering with you.
(Correct): C
(Points): 1
(CF): You do not want to try and lay blame, and trying to get people in trouble burns bridges.  Mimicking the anger is clearly bad too.
(WF): You do not want to try and lay blame, and trying to get people in trouble burns bridges.  Mimicking the anger is clearly bad too.
(STARTIGNORE)
(Hint):
(Subject): Communications Interpersonal
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


