(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jason Crocker
(Course Site): Scrum.org
(Course Name): Scrum Guide
(Course URL): https://www.scrum.org/Portals/0/Documents/Scrum%20Guides/2013/Scrum-Guide.pdf
(Discipline):Professional 
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum is founded on the Empirical Process Control Theory, in that Scrum focuses on�
(A): Completing projects in large segments, completing each section before moving on.
(B): Creating multiple approaches simultaneously and choosing the best at the end.
(C): Having a testing phase after each phase, only moving on after the end of the previous phase.
(D): Working on small segments in a short timeframe, adding on as new requirements emerge.
(E): A four phase cycle that is continually passed through until completion.
(Correct): D
(Points): 1
(CF): 
(WF): Scrum is an agile development framework, if focuses on small sprints, back to back, only changing the project details between sprints.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): There are three pillars that the empirical process stands are, what are they?
(A): Transparency
(B): Recursion
(C): Inspection
(D): Obfuscation
(E): Adaptation
(Correct): A,C,E
(Points): 1
(CF): 
(WF): Transparency or having all aspects defined by a common standard to promote a common understanding. Inspection, this is used to find any undesirable elements during or after a sprint. After an inspection if an undesirable element is found the project must adapt to change to remove the unwanted elements. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): As part of the Daily Scrum meetings each member must do a stand up presentation covering these three elements.
(A): Who am I working with?
(B): What where any blockers to achieving this?
(C): What did I do yesterday?
(D): What am I doing this sprint?
(E): What am I going to do tomorrow?
(Correct): B,C,E
(Points): 1
(CF): 
(WF): These short meeting are daily to cover what was done between the current and the last one, what is planed to be done before the next and if something is stopping you what it is so it can be dealt with.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): The members that make up the Scrum Team are?
(A): Team Owner
(B): Product Owner
(C): Scrum Team
(D): Development Team
(E): Scrum Master
(Correct): B,D,E
(Points): 1
(CF): 
(WF): The three parts that make the Scrum team are, the Product Owner, the Development Team and the Scrum Master.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): The Product Owner is responsible for what part of the project?
(A): They are the financial backers responsible for funding.
(B): The are responsible for managing the Product Backlog.
(C): The are responsible for managing the Development Team during the sprints.
(D): They are responsible for the Sprint Backlog.
(E): All of the Above
(Correct): B
(Points): 1
(CF): 
(WF): The Product Owner is responsible for maximizing the value of the product and the work of the Development Team, they manage the Product Backlog in order to achieve this. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): At the end of a Sprint the Development Team should have what?
(A): A working version of the product.
(B): Whatever they happen to finish by the end of the Sprint.
(C): The final product.
(D): An approximate layout for the product.
(E): A plan for the next Sprint.
(Correct): A
(Points): 1
(CF): 
(WF): At the end of a Sprint the Development Team should have a working version of a product, there should always be a working version of the product regardless of its level of completeness.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who is accountable in a Development Team?
(A): The person in the team who was elected leader.
(B): The Scrum Master
(C): The Product Owner
(D): The Development Team
(E): The Scrum Team
(Correct): D
(Points): 1
(CF): 
(WF): There are no titles within a Development Team, and everyone in the team is accountable for the team. The Scrum Master and Product Owner are not part of the Development Team but rather part of the Scrum Team.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What is the Scrum Master responsible for?
(A): Making sure the principles of Scrum are followed at all times.
(B): Leading the Development Team.
(C): Removing impediments to the Development Team's progress.
(D): Creating the Sprint Backlog.
(E): Modifying the Product Backlog.
(Correct): A,C
(Points): 1
(CF): 
(WF):The Scrum Master is there to facilitate Scrum and help the team follow Scrum principles, he/she also removes impediments so the Development Team can continue working. Only the Product Owner can modify the Product Backlog. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner 
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the longest and shortest time block a Sprint should have.
(A): 1 Year, 1 Month
(B): 1 Month, 8 Hours
(C): 1 Month, 1 Week
(D): 1 Week, 1 Day
(E): 1 Year, 6 Months
(Correct): C
(Points): 1
(CF): 
(WF): It is advised that a Sprint be no longer that 1 month to prevent it from being overly complex, and it should be no shorter than a week as less makes it hard to accomplish anything.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What is true about canceling a sprint?
(A): You can not cancel a Sprint.
(B): Only the Product Owner can cancel a Sprint.
(C): You cancel Sprints if they will not be finished in time.
(D): You cancel Sprints if the Sprint Goal becomes obsolete.
(E): Cancelation of Sprints is a regular occurrence.
(Correct): B,D
(Points): 1
(CF): 
(WF): Cancelation of Sprints is not regular occurrence, and usually is only done when the Sprint Goal is obsolete and can only be done by the Product Owner. The Product Owner can take input from other members of the Scrum Team, but the final decision is his/hers.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of these are true about the Sprint Backlog?
(A): The Product Owner creates the Sprint Backlog.
(B): The Sprint Backlog is created during the Sprint Planning.
(C): The Sprint Backlog is the combination of the Product Backlog items selected for the Sprint plus the plan for delivering them.
(D): The Sprint Backlog can be changed in the middle of the Sprint.
(E): The Scrum Master approves the Sprint Backlog before it is implemented.
(Correct): B,C
(Points): 1
(CF): 
(WF):The Sprint Backlog is the Product Backlog items to be covered during the sprint combined with the plan for delivering the items created during the Sprint Planning.  
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What is true about a Sprint Review?
(A): Anyone can come to the Sprint Review. 
(B): The Product Owner explains what Product Backlog items have been completed.
(C): The Development Team talks about what went well and what problems where run into during the Sprint.
(D): The next Sprint Backlog is started.
(E): The Development Team shows what was completed during the Sprint.
(Correct): B,C,E
(Points): 1
(CF): 
(WF): The Sprint Review is used to review the previous Sprint. The Scrum Team, and anyone the Product Owner invites attends these meetings. The work from the Sprint is shown and talked about as well as discussing the next Sprint providing input into subsequent Sprint Planning.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): The goals of the Sprint Retrospective are?
(A): Inspect how the last Sprint went with regards to people, relationships, process, and tools.
(B): Figure out who did not complete there section of the Sprint.
(C): Identify and order the major items that went well and potential improvements.
(D): Look back on the Sprint to access where the team deviated from the plan.
(E): Create a plan for implementing improvements to the way the Scrum Team does it work.
(Correct): A,C,E
(Points): 1
(CF): 
(WF): The main goal of the retrospective is to look back at the Sprint and analyze what worked and did not and make a plan to fix the things that did not work.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of these is not true about the Product Backlog?
(A): The Product Backlog is an ordered list of everything that might be needed in the product.
(B): The items on the Product Backlog are locked, only the order of importance changes. 
(C): The Product Backlog evolves as the product and the environment in which it will be used evolves.
(D): As long as a product exists, its Product Backlog also exists.
(E): A Product Backlog is never complete.
(Correct): B
(Points): 1
(CF): 
(WF): Items on the Product Backlog can be removed or modified with the evolution of the Product Backlog. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of these is not true about the Agile method?
(A): Agile is a flexible method of software development.
(B): Agile can be customized to fit each team and project separately. 
(C): Scrum and Kanban are examples of different forms that the Agile method can take.
(D): Pair Programming is a requirement for any of the Agile methods.
(Correct): D
(Points): 1
(CF): 
(WF): Pair Programming is a part of some Agile methods, like Extreme Programming but is not required for all Agile methods. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of these are the types of items found on the Product Backlog.
(A): Features
(B): Tasks
(C): Bugs
(D): Technical Work
(E): Knowledge Acquisition
(Correct): A,C,D,E
(Points): 1
(CF): 
(WF): Tasks are created from the Product Backlog and are not explicitly on the Product Backlog.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of these is not true about Planning Poker?
(A): The numbers on the cards can represent a unit of time agreed upon by each group of estimators.
(B): All cards are selected individually in private and revealed simultaneously.
(C): If all estimators selected the same value that becomes the estimate.
(D): If not all estimators selected the same value they discuss their estimates.
(E): If the estimators do not agree after five times then the estimate with the majority is selected.
(Correct): E
(Points): 1
(CF): 
(WF): The planning poker goes on until all the estimators agree upon the estimate, at no point does majority select the estimate.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are the typical columns on a Scrum Task Board?
(A): Story
(B): To Do
(C): Work in Process
(D): Done
(E): No Longer Needed
(Correct): A,B,C,D
(Points): 1
(CF): 
(WF): The no longer needed section is redundant as you would just remove a card from the board if it was not needed for the Sprint.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of these are true about User Stories?
(A): The typical template of a User Story is, As a <type of user>, I want <some goal> so that <some reason>.
(B): Details can be added to user stories by, splitting a user story into multiple smaller stories.
(C): A User Story should utilize technical language.
(D): User Stories should be rich with information to describe the feature.
(E): Details can be added to user stories by adding conditions of satisfaction.
(Correct): A,B,E
(Points): 1
(CF): 
(WF): A User Story is a short simple description of a feature told from the perspective of the person who desires the new capability.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


